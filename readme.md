# devup.cmd v1.4

A simple Windows cmd script to start up the vagrant environment.

Takes the following steps:

  * Perform an incremental backup of the database files first
  * Change cmd line to correct working directory
  * Start vagrant

The database backup is because sometimes VVV or Vagrant can self implode. It starts a
reprovision, stops being able to understand the databases and can overwrite the files.

Automatically updates the vagrant box after launching it.


## Configuration
Will need various paths updating if the folder structure changes:

  * `.\devup.cmd`
  * `.\cmder-stub\devup.cmd`
  * `.\backup\settings.txt`
  * `.\backup\inclusions.txt`


## Installation
Follow these steps:

  1. Update paths listed in the configuration section above
  2. Drop the `.\cmder-stub\devup.cmd` into the cmder folder located at
   `C:\cmder\bin` otherwise it will have problems finding the script.

Note: If cmder is installed in Program Files then move it to root or some other
location because it can cause problems with file permissions.

## macOS Equivalent
Create an alias in your `.zshrc` file:

```shell
code ~/.zshrc
```

Then add in this alias, adjusting the path as required:

```
# custom alias to support the devup command
devup() {
  cd ~/rtp/rtpDesign/localDev/
  vagrant up
}
```

## Release History
v1.4 - 2017 06 01 - Updated paths to move VM to localdev

v1.3 - 2016 04 03 - Updated docs to work with Cmder v1.2

v1.2 - 2015 09 22 - Added auto vagrant box update at the end of launch

v1.1 - 2015 09 05 - Added incremental database backup

v1.0 - 2015 04 01 - Initial version of the script


## Acknowledgements
Thanks to the people that posted this information:  
  
  - [Backup script provided Logaan](http://www.logaans-site.co.uk/2009/10/17/simpler-version-of-the-7-zip-backup/)

  - [Running a cmd from a cmd](http://stackoverflow.com/questions/1103994/how-to-run-multiple-bat-files-within-a-bat-file)

  - [Adding comments in cmd file](http://superuser.com/questions/82231/how-do-i-do-comments-at-a-windows-command-prompt)
  
  - [Relative paths in cmd](http://stackoverflow.com/questions/14936625/relative-path-in-bat-script)


## Credits
Matthew Harris, rtpHarry, matthew@rtpdesign.co.uk

rtp design ltd, https://www.rtpdesign.co.uk/

