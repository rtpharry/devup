# The path to the where the 7z.exe executable is
varPathToSevenZip=C:\Program Files\7-Zip

# The path where to store the backup sets
varBackupPath=E:\rtpSoftware\ShellScripts\devup\backups

# The file path to the inclusions file
varInclusionsFile=E:\rtpSoftware\ShellScripts\devup\backup\inclusions.txt

# The file path to the exclusions file
varExclusionsFile=E:\rtpSoftware\ShellScripts\devup\backup\exclusions.txt

# Whether to wait at the end of the backup
varWaitAtEnd=false

# Type of backup archive to create
varArchiveType=zip